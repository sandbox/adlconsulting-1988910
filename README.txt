Module: UC_Lead_Tracking
Author: ADL Consulting Ltd <http://drupal.org/user/2557962>


Description
===========
Enables lead source tracking in Ubercart 3 on D7.

Requirements
============

* Drupal 7
* Ubercart 3


Installation
============
* Copy the 'uc_lead_tracking' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
Start by visiting: /admin/store/settings/checkout

1.) You can enable/disable the lead tracking pane under "Basic Settings"
2.) Use the "UC Lead Tracking pane" to set title of the select box - e.g. "How did you hear about us"
3.) Fill in the options you would like available.

You will now have a select field available at the checkout phase.

To view reports, go to: /admin/store/reports/uc_lead_tracking_report

Here you can click on an option to see the orders that came from that source.

Authors
-------
ADL Consulting (http://www.adl-consulting.co.uk)
Dong Le (http://lenamsoft.com)

If you use this module, find it useful, and want to send the authors a thank
you note, then use the Feedback/Contact page at the URLs above.

For paid customizations of this and other modules, please contact ADL Consulting Ltd
